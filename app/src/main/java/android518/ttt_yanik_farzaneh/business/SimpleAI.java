package android518.ttt_yanik_farzaneh.business;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * The SimpleAI implements the AI interface and will generate a random move
 * when asked to compute a move
 *
 * @author Yanik
 * @version 2018-10-14
 */
public class SimpleAI implements AI, Serializable {
    private static final Random RANDOM = new Random();
    private TicTacToe game;
    private Player droidPlayer;
    private Player enemyPlayer;

    public SimpleAI(TicTacToe game, Player droidPlayer) {
        this.game = game;
        this.droidPlayer = droidPlayer;
        this.enemyPlayer = determineEnemyPlayer();
    }

    /**
     * Helper method to determine what player is the enemy player in the game
     */
    private Player determineEnemyPlayer() {
        if (game.getPlayer1().equals(droidPlayer) == false) {
            return game.getPlayer1();
        } else {
            return game.getPlayer2();
        }
    }

    @Override
    public Move computeMove() {
        return generateRandomMove(game.getBoard());
    }

    /**
     * Helper method to generate a random move
     * i.e. randomly choose a cell that has a NO_MOVE_YET mark
     */
    private Move generateRandomMove(Board board) {
        // Converting board into 1D array list so that it will be easier to generate a random move
        List<Cell> board1D = new ArrayList<>();
        for (int col = 0; col < board.getBoardSize(); col++) {
            for (int row = 0; row < board.getBoardSize(); row++) {
                board1D.add(board.getCell(col, row));
            }
        }

        // Getting the positions of the spots that contain a NO_MOVE_YET code in an array list
        // So that we can simply generate a random index that will represent the NO_MOVE_YET position
        List<Integer> noMoveYetPositions = new ArrayList<>();
        for (int i = 0; i < board1D.size(); i++) {
            if (board1D.get(i).getMark() == Mark.NO_MOVE_YET) {
                noMoveYetPositions.add(i);
            }
        }

        int randomIndex = RANDOM.nextInt(noMoveYetPositions.size());
        int randomPosition = noMoveYetPositions.get(randomIndex);

        int col = randomPosition / board.getBoardSize();
        int row = randomPosition - (col * board.getBoardSize());

        return new Move(col, row);
    }
}
