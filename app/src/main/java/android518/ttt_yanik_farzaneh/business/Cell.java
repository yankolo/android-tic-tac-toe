package android518.ttt_yanik_farzaneh.business;

import java.io.Serializable;

/**
 * The Cell represents one cell in the board. It has a Mark.
 *
 * By default when created, cells are initialized with a NO_MOVE_YET mark.
 *
 * @author Yanik
 * @version 2018-10-14
 */
public class Cell implements Serializable {
    private Mark mark;

    public Cell() {
        this.mark = Mark.NO_MOVE_YET;
    }

    /**
     * Gets the mark that is contained in the Cell
     * @return The mark that is contained in the Cell
     */
    public Mark getMark() {
        return mark;
    }

    /**
     * Sets the mark that should be contained in the Cell
     * @param mark The mark that should be contained in the Cell
     */
    public void setMark(Mark mark) {
        this.mark = mark;
    }
}
