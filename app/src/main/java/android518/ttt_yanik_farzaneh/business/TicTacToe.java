package android518.ttt_yanik_farzaneh.business;

import java.io.Serializable;

/**
 * This is the actual class that represents the game.
 * We can think of it as the model. If some part of
 * an application (other than the business classes)
 * wants to interact with the game, it should only do so
 * through the TicTacToe class. The TicTacToe class contains
 * all the information necessary to represent a game visually
 * with a GUI (or with anything else).
 *
 * @author Yanik
 * @version 2018-10-14
 */
public class TicTacToe implements Serializable {
    public enum GameState {PLAYER_1_PLAYING, PLAYER_2_PLAYING, PLAYER_1_WON, PLAYER_2_WON, TIE};
    private GameState gameState;

    private Player player1;
    private Player player2;

    // 2D array that represents the tic tac toe game
    private Board board;

    public TicTacToe(boolean againstDroid) {
        this.player1 = new Player(Mark.PLAYER_1, "Player 1");
        if (againstDroid) {
            AI ai = new SimpleAI(this, this.player2);
            this.player2 = new Player(Mark.DROID, "Droid", ai);
        } else {
            this.player2 = new Player(Mark.PLAYER_2, "Player 2");
        }

        this.board = new Board(3);

        this.gameState = GameState.PLAYER_1_PLAYING;
    }

    /**
     * Method that is used to play a move in the Tic Tac Toe game
     * The current player mark is placed into the specified cell
     *
     * If the game is played against droid, the droid will play a move right after the
     * specified move is played
     *
     * @param column The column of the cell to mark
     * @param row The row of the cell to mark
     * @throws IllegalArgumentException Exception is thrown if move is invalid
     * @throws IllegalStateException Exception is thrown if play() is called when game has
     *                              already ended
     */
    public void play(int column, int row) throws IllegalArgumentException, IllegalStateException {
        if (isGameEnded()) {
            throw new IllegalStateException("Cannot play move because game has already ended");
        }

        Player currentPlayer = getCurrentPlayingPlayer();

        Move movePlayed = new Move(column, row);
        board.markCell(movePlayed, currentPlayer.getPlayerMark());

        if (board.checkForWin(currentPlayer)) {
            currentPlayerWon();
        } else if (board.verifyIfBoardIsFull()) {
            gameState = GameState.TIE;
        }

        if (isGameEnded() == false) {
            passTurnToNextPlayer();
            currentPlayer = getCurrentPlayingPlayer();

            if (currentPlayer.isDroid()) {
                Move aiMove = currentPlayer.getAI().computeMove();
                play(aiMove.getColumn(), aiMove.getRow());
            }
        }
    }

    /**
     * Helper method to pass the turn to the next player
     * i.e. change the currently playing player
     */
    private void passTurnToNextPlayer() {
        if (gameState == GameState.PLAYER_1_PLAYING) {
            gameState = GameState.PLAYER_2_PLAYING;
        } else if (gameState == GameState.PLAYER_2_PLAYING) {
            gameState = GameState.PLAYER_1_PLAYING;
        }
    }

    /**
     * Helper method to change the state of the game so that the current
     * player has won the game
     */
    private void currentPlayerWon() {
        if (gameState == GameState.PLAYER_1_PLAYING) {
            gameState = GameState.PLAYER_1_WON;
        } else if (gameState == GameState.PLAYER_2_PLAYING) {
            gameState = GameState.PLAYER_2_WON;
        }
    }

    /**
     * Gets the Player whose turn it is currently
     * @return The Player whose turn it is currently (null
     *          if no player currently has a turn)
     */
    public Player getCurrentPlayingPlayer() {
        Player player = null;

        if (gameState == GameState.PLAYER_1_PLAYING) {
            player = player1;
        } else if (gameState == GameState.PLAYER_2_PLAYING) {
            player = player2;
        }

        return player;
    }

    /**
     * Method used to determine if the game has ended
     * @return True if the game has ended
     */
    public boolean isGameEnded() {
        return gameState == GameState.PLAYER_1_WON || gameState == GameState.PLAYER_2_WON ||
                gameState == GameState.TIE;
    }

    /**
     * Gets the Player that won the game
     * @return The Player that won the game (null if none)
     */
    public Player getPlayerThatWon() {
        Player player = null;

        if (gameState == GameState.PLAYER_1_WON) {
            player = player1;
        } else if (gameState == GameState.PLAYER_2_WON) {
            player = player2;
        }

        return player;
    }

    /**
     * Gets the GameState of the game
     * @return The GameState of the game
     */
    public GameState getGameState() { return gameState; }

    /**
     * Gets Player 1
     * @return Player 1
     */
    public Player getPlayer1() {
        return player1;
    }

    /**
     * Gets Player 2
     * @return Player 2
     */
    public Player getPlayer2() {
        return player2;
    }

    /**
     * Method used to get the TicTacToe board
     * @return The TicTacToe board
     */
    public Board getBoard() {
        return board;
    }
}
