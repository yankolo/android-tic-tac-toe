package android518.ttt_yanik_farzaneh.business;

/**
 * The interface that defines the public methods of an AI.
 * Useful if we would like to add different AIs in the future.
 *
 * @author Yanik
 * @version 2018-10-14
 */
public interface AI {
    /**
     * This method computes a move
     *
     * @return A computed move
     */
    public Move computeMove();
}
