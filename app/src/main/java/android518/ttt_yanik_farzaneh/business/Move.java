package android518.ttt_yanik_farzaneh.business;

import java.io.Serializable;

/**
 * A simple class that represents a move. We can think
 * of a move as an "intent" to mark a cell. A move contains
 * a column and a row (which map to a specific cell in the board)
 *
 * Note, there is no validation in the Move class.
 *
 * @author Yanik
 * @version 2018-10-14
 */
public class Move implements Serializable {
    private int column;
    private int row;

    public Move() {}

    public Move(int column, int row) {
        this.column = column;
        this.row = row;
    }

    /**
     * Gets the column of the move
     * @return The column of the move
     */
    public int getColumn() {
        return column;
    }

    /**
     * Sets the column of the move
     * @param column The column of the move to set
     */
    public void setColumn(int column) {
        this.column = column;
    }

    /**
     * Gets the row of the move
     * @return The row of the move
     */
    public int getRow() {
        return row;
    }

    /**
     * Sets the row of the move
     * @param row The row of the move to set
     */
    public void setRow(int row) {
        this.row = row;
    }
}
