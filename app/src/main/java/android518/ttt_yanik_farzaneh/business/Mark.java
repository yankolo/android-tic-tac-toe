package android518.ttt_yanik_farzaneh.business;

/**
 * A mark represents a value that can be inserted inside a ce
 *
 * A mark distinguishes which player "marked" the Cell.
 *
 * Think of it as the "O" or "X" in a real Tic Tac Toe game.
 * However, the Tic Tac Toe board cells contain more information than the "O" and "X",
 * of a real game. For example, a cell can contain a value that represent the fact that
 * there wasn't a move in the cell yet (NO_MOVE_YET), a value that represents
 * the fact that a droid has played a move in the cell (DROID), a value that
 * represents the fact that player 1 played a move in the cell (PLAYER_1), etc.
 *
 * @author Yanik
 * @version 2018-10-14
 */
public enum Mark {
    NO_MOVE_YET,
    PLAYER_1,
    PLAYER_2,
    DROID;
}
