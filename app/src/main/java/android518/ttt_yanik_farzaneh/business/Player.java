package android518.ttt_yanik_farzaneh.business;

import java.io.Serializable;
import java.util.Objects;

/**
 * The player class represents a Tic Tac Toe player
 *
 * The player encapsulates all the information about
 * the player. It is useful to separate this information,
 * as it will be easy for us to extend players in the future.
 * In addition, a player can be a Droid, if it is droid an AI
 * should be provided in the player constructor.
 *
 * The playerMark is the Mark that will be used when setting
 * a cell mark in the board
 *
 * @author Yanik
 * @version 2018-10-14
 */
public class Player implements Serializable {
    private Mark playerMark;
    private String name;
    private AI ai;
    private boolean isDroid;

    public Player(Mark playerMark, String name, AI ai) {
        this.playerMark = playerMark;
        this.name = name;
        this.ai = ai;
        this.isDroid = true;
    }

    public Player(Mark playerMark, String name) {
        this.playerMark = playerMark;
        this.name = name;
        this.isDroid = false;
    }

    /**
     * Gets the Mark that represents the player
     * @return The Mark that represents the player
     */
    public Mark getPlayerMark() {
        return playerMark;
    }

    /**
     * Gets the name of the player
     * @return The name of the player
     */
    public String getName() {
        return name;
    }

    /**
     * Returns whether the player is a droid or not
     * @return True if player is a droid
     */
    public boolean isDroid() {
        return isDroid;
    }

    /**
     * Gets the AI of the player
     * @return The AI of the player (null if none)
     */
    public AI getAI() {
        return ai;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return isDroid == player.isDroid &&
                playerMark == player.playerMark &&
                Objects.equals(name, player.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(playerMark, name, isDroid);
    }
}
