package android518.ttt_yanik_farzaneh.business;

import java.io.Serializable;

/**
 * The board contains all the cells of the game.
 * It also provides methods to verify whether a
 * player won, or if the board is full.
 *
 * @author Yanik
 * @version 2018-10-14
 */
public class Board implements Serializable {
    private Cell[][] cells;
    private final int boardSize;

    public Board(int boardSize){
        this.boardSize = boardSize;
        initializeBoard();
    }

    /**
     * Helper method to initialize the board cells
     */
    private void initializeBoard() {
        this.cells = new Cell[boardSize][];
        for (int i = 0; i < boardSize; i++){
            this.cells[i] = new Cell[boardSize];
            for (int j = 0; j < this.cells[i].length; j++) {
                this.cells[i][j] = new Cell();
            }
        }
    }

    /**
     * This method marks the appropriate cell (specified in the move) with the specified mark
     *
     * @param move The move that contains the coordinates of the cell to mark
     * @param mark The mark to use when marking the cell
     * @throws IllegalArgumentException Exception is thrown if move is not valid (out of bounds)
     */
    public void markCell(Move move, Mark mark) throws IllegalArgumentException {
        validateMove(move);
        cells[move.getColumn()][move.getRow()].setMark(mark);
    }

    /**
     * This method verifies if a specific player won the game
     *
     * Note, this method works only with 3x3 boards
     * @param player The player whose mark is used to find a win
     * @return True if there is a column, row, or diagonal that only contains the
     *          specified mark (win)
     */
    public boolean checkForWin(Player player) {
        Mark mark = player.getPlayerMark();

        for (Cell[] col: cells) {
            if (col[0].getMark() == mark && col[1].getMark() == mark
                    && col[2].getMark() == mark) {
                return true;
            }
        }

        for (int row = 0; row < boardSize; row++){
            if (cells[0][row].getMark() ==  mark && cells[1][row].getMark() ==  mark
                    && cells[2][row].getMark() ==  mark) {
                return true;
            }
        }

        // Creating a diagonals 2D array which will contain the diagonals of the tic tac toe game
        // This is done so we could loop through the diagonals and simply perform a win check
        // (like with the columns and rows)
        Cell[][] diagonals = new Cell[2][];

        diagonals[0] = new Cell[3];
        for (int i = 0, col = 0, row = 0; i < diagonals[0].length; i++, col++, row++) {
            diagonals[0][i] = cells[col][row];
        }

        diagonals[1] = new Cell[3];
        for (int i = 0, col = 0, row = 2; i < diagonals[1].length; i++, col++, row--) {
            diagonals[1][i] = cells[col][row];
        }

        for (Cell[] diagonal: diagonals) {
            if (diagonal[0].getMark() ==  mark && diagonal[1].getMark() ==  mark
                    && diagonal[2].getMark() ==  mark) {
                return true;
            }
        }

        // Returns false if none of the checks above for a win return true
        return false;
    }

    /**
     * This method verifies if the board is full (i.e. all cells have a been marked)
     * @return True if board is full
     */
    public boolean verifyIfBoardIsFull() {
        for (Cell[] col: cells) {
            for (Cell cell: col) {
                if (cell.getMark() == Mark.NO_MOVE_YET) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Gets all the Cells of the Board
     * @return All the Cells of the Board
     */
    public Cell[][] getCells() {
        return cells;
    }

    /**
     * Gets the specified Cell
     * @param column The column of the board
     * @param row The row of the board
     * @return The cell that is contained in the specified column and row
     */
    public Cell getCell(int column, int row) {
        validateInBounds(column, row);
        return cells[column][row];
    }

    /**
     * Gets the board size (always square)
     * @return The board size
     */
    public int getBoardSize() {
        return boardSize;
    }

    /**
     * Helper method to validate if a move is valid
     *
     * @param move The move to validate
     * @throws IllegalArgumentException Exception is thrown if move is out of bounds
     */
    private void validateMove(Move move) throws IllegalArgumentException {
        validateInBounds(move.getColumn(), move.getRow());
    }

    /**
     * Helper method to verify if a column and row are in the bounds of the board
     *
     * @param column The column to verify
     * @param row The row to verify
     * @throws IllegalArgumentException Exception is thrown if not in bounds
     */
    private void validateInBounds(int column, int row) throws IllegalArgumentException {
        if (column < 0 || column >= boardSize) {
            throw new IllegalArgumentException("Column number can only be between 0 " +
                    "(inclusive) and " + boardSize + " (inclusive)");
        } else if (row < 0 ||row >= boardSize) {
            throw new IllegalArgumentException("Row number can only be between 0 " +
                    "(inclusive) and " + boardSize + " (inclusive)");
        }
    }
}
