package android518.ttt_yanik_farzaneh;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

/**
 * The about Activity
 *
 * @author Farzaneh
 * @version 2018-10-14
 */
public class AboutActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        TextView aboutGameTextView = (TextView) findViewById(R.id.about_game_message);
        String aboutGameMessage = getIntent().getExtras().getString("about_game_message");

        aboutGameTextView.setText(aboutGameMessage);
    }
}
