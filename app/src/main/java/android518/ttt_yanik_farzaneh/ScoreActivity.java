package android518.ttt_yanik_farzaneh;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.TextView;

/**
 * The Score activity
 *
 * @author Farzaneh & Yanik
 * @version 2018-10-14
 */
public class ScoreActivity extends Activity{
    private int player1WinCount;
    private int player2WinCount;
    private int droidWinCount;
    private int resetCount;
    private int tieCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);

        TextView tvPlayer1WinCount = (TextView) findViewById(R.id.player_1_wins_count);
        TextView tvPlayer2WinCount = (TextView) findViewById(R.id.player_2_wins_count);
        TextView tvDroidWinCount = (TextView) findViewById(R.id.droid_wins_count);
        TextView tvResetCount = (TextView) findViewById(R.id.resets_count);
        TextView tvTieCount = (TextView) findViewById(R.id.tie_count);

        if (savedInstanceState != null) {
            this.player1WinCount = savedInstanceState.getInt("player1Win");
            this.player2WinCount = savedInstanceState.getInt("player2Win");
            this.droidWinCount = savedInstanceState.getInt("droidWin");
            this.resetCount = savedInstanceState.getInt("resetCount");
            this.tieCount = savedInstanceState.getInt("tieCount");
        } else {
            SharedPreferences preferences = getSharedPreferences("counters_preferences", MODE_PRIVATE);
            this.player1WinCount = preferences.getInt("player1Win", 0);
            this.player2WinCount = preferences.getInt("player2Win", 0);
            this.droidWinCount = preferences.getInt("droidWin", 0);
            this.resetCount = preferences.getInt("resetCount", 0);
            this.tieCount = preferences.getInt("tieCount", 0);
        }

        tvPlayer1WinCount.setText(Integer.toString(player1WinCount));
        tvPlayer2WinCount.setText(Integer.toString(player2WinCount));
        tvDroidWinCount.setText(Integer.toString(droidWinCount));
        tvResetCount.setText(Integer.toString(resetCount));
        tvTieCount.setText(Integer.toString(tieCount));
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

        savedInstanceState.putInt("player1Win", this.player1WinCount);
        savedInstanceState.putInt("player2Win", this.player2WinCount);
        savedInstanceState.putInt("droidWin", this.droidWinCount);
        savedInstanceState.putInt("resetCount", this.resetCount);
        savedInstanceState.putInt("tieCount", this.tieCount);
    }
}
