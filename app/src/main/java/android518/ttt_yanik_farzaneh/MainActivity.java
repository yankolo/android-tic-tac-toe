package android518.ttt_yanik_farzaneh;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import android518.ttt_yanik_farzaneh.business.Board;
import android518.ttt_yanik_farzaneh.business.Cell;
import android518.ttt_yanik_farzaneh.business.Mark;
import android518.ttt_yanik_farzaneh.business.TicTacToe;

/**
 * The Activity that is launched when the app is started.
 * A visual representation of the TicTacToe business class
 *
 * @author Farzaneh & Yanik
 * @version 2018-10-14
 */
public class MainActivity extends Activity {

    private ImageButton[][] imageButtons;
    private TextView againstTextView;
    private TicTacToe game;
    private Drawable notPlayedDrawable;
    private Drawable player1MoveDrawable;
    private Drawable player2MoveDrawable;
    private Drawable droidMoveDrawable;
    private int player1WinCount;
    private int player2WinCount;
    private int droidWinCount;
    private int resetCount;
    private int tieCount;

    // Boolean to track if the end of game dialog or toast
    // was already displayed. Without it updateGame() always displays the "end
    // of game" (even if it already displayed it) when it detects that the game has ended.
    private boolean isEndOfGameDisplayed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences preferences = getSharedPreferences("counters_preferences", MODE_PRIVATE);
        this.player1WinCount = preferences.getInt("player1Win", this.player1WinCount);
        this.player2WinCount = preferences.getInt("player2Win",this.player2WinCount);
        this.droidWinCount = preferences.getInt("droidWin", this.droidWinCount);
        this.resetCount = preferences.getInt("resetCount", this.resetCount);
        this.tieCount = preferences.getInt("tieCount", this.tieCount);

        this.imageButtons = getImageButtons();
        this.againstTextView = (TextView) findViewById(R.id.against) ;
        this.game = new TicTacToe(true);
        this.notPlayedDrawable = getDrawable(R.drawable.notplayed);
        this.player1MoveDrawable = getDrawable(R.drawable.cross);
        this.player2MoveDrawable = getDrawable(R.drawable.circle);
        this.droidMoveDrawable = getDrawable(R.drawable.triangle);

        if (savedInstanceState != null) {
            this.game = (TicTacToe) savedInstanceState.getSerializable("tic_tac_toe_game");
            this.isEndOfGameDisplayed = savedInstanceState.getBoolean("end_of_game_displayed");
        } else {
            this.game = new TicTacToe(true);
        }

        updateGame();
    }//end onCreate()

    @Override
    protected void onPause(){
        super.onPause();

        // Store the counters in the SharedPreferences
        SharedPreferences preferences = getSharedPreferences("counters_preferences", MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("player1Win", this.player1WinCount);
        editor.putInt("player2Win",this.player2WinCount);
        editor.putInt("droidWin", this.droidWinCount);
        editor.putInt("resetCount", this.resetCount);
        editor.putInt("tieCount", this.tieCount);
        editor.commit();
    }//end onPause

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

        savedInstanceState.putSerializable("tic_tac_toe_game", game);
        savedInstanceState.putBoolean("end_of_game_displayed", isEndOfGameDisplayed);
    }

    /**
     * Retrieves the row and col from the clicked imageButton's tag, plays game with these coordinate
     * then calls updateGame() to update the game GUI and necessary values.
     * @param view An ImageButton in UI
     */
    public void imageButtonClickHandler(View view) {
        String cellTag = view.getTag().toString();
        int row = Integer.parseInt(cellTag.substring(0,1));
        int col = Integer.parseInt(cellTag.substring(1, 2));

        this.game.play(col, row);
        updateGame();
    }//end imageButtonClickHandler()

    /**
     * Reset button click handler. Reset board and instantiate a new game
     * @param view the reset button in UI
     */
    public void resetGame(View view) {
        this.resetCount++;
        //Keep the same second player, initialize new game and reset board
        if(this.game.getPlayer2().isDroid())
            this.game = new TicTacToe(true);
        else
            this.game = new TicTacToe(false);

        // Set isEndOfGameDisplayed to false to reset it (see comment in
        // beginning of class for explanation of what it does)
        isEndOfGameDisplayed = false;

        updateGame();

        Toast.makeText(this, R.string.game_reset_toast,
                Toast.LENGTH_SHORT).show();
    }//end resetGame()

    /**
     * About button click handler, starts AboutActivity
     * @param view the about button in UI
     */
    public void launchAboutActivity(View view) {
        Intent i = new Intent(this, AboutActivity.class);

        String aboutGameMessage = null;
        if (game.isGameEnded() == false) {
            // Getting string that will display the player whose turn it is in the about activity
            String currentTurnResourceMessage = getResources().getString(R.string.current_turn_text);

            // currentTurnResourceMessage contains a string format (%s) where the
            // player name should be placed
            aboutGameMessage = String.format(currentTurnResourceMessage, game.getCurrentPlayingPlayer().getName());
        } else {
            // If game has ended, instead of displaying the player whose turn it is, display
            // message saying that the game has ended
            aboutGameMessage = getResources().getString(R.string.game_currently_ended);
        }

        i.putExtra("about_game_message", aboutGameMessage);
        startActivity(i);
    }

    /**
     * Zero button click handler, resets all the counters
     * @param view the Zero button in UI
     */
    public void resetCounters(View view) {
        this.player1WinCount = 0;
        this.player2WinCount = 0;
        this.droidWinCount = 0;
        this.resetCount = 0;
        this.tieCount = 0;

        Toast.makeText(this, R.string.counters_reset_toast,
                Toast.LENGTH_SHORT).show();
    }//end resetCounter()

    /**
     * Score button click handler, starts ScoreActivity
     * @param view score button in UI
     */
    public void launchScoreActivity(View view) {
        Intent i = new Intent(this, ScoreActivity.class);
        startActivity(i);
    }

    /**
     * Play button click handler. It switches the game mode (from droid to a human player and vise versa).
     * @param view the play button in UI
     */
    public void switchPlayers(View view) {
        if (this.game.getPlayer2().isDroid()) {
            this.game = new TicTacToe(false);
        } else {
            this.game = new TicTacToe(true);
        }

        // Set isEndOfGameDisplayed to false to reset it (see comment in
        // beginning of class for explanation of what it does)
        isEndOfGameDisplayed = false;

        updateGame();

        Toast.makeText(this, R.string.switch_players_toast,
                Toast.LENGTH_SHORT).show();
    }//end switchPlayers()

    /**
     * This method finds all the image buttons of the tic tac toe game and creates a 2D
     * array that represents the game with those image buttons
     * @return A 2D array with the image buttons in the GUI
     */
    private ImageButton[][] getImageButtons(){
        int[][] btnIds =  {
                {R.id.cell_00, R.id.cell_10, R.id.cell_20}, // First column
                {R.id.cell_01, R.id.cell_11, R.id.cell_21}, // Second column
                {R.id.cell_02, R.id.cell_12, R.id.cell_22} // Third column
        };

        ImageButton[][] buttons = new ImageButton[btnIds.length][];
        for(int row=0; row<btnIds.length; row++){
            buttons[row] = new ImageButton[btnIds[row].length];
            for(int col=0; col<buttons.length; col++){
                buttons[row][col] = (ImageButton) findViewById(btnIds[row][col]);
            }
        }
        return buttons;
    }//end getImageButtons()

    /**
     * Keeps the GUI and TicTacToe business object in sync.
     * It also increments the appropriate counters.
     */
    private void updateGame() {
        updateAgainstTextView();
        updateButtonDrawables();

        if(game.isGameEnded() == false) {
            updateButtonClickableState();
        } else {
            displayEndOfGameMessage();
            incrementGameCounters();
            disableAllButtons();
        }
    }//end updateGame()

    /**
     * Helper method to update the TextView that displays
     * the game mode and whose turn it is.
     */
    private void updateAgainstTextView() {
        if (game.isGameEnded()) {
            againstTextView.setText(R.string.the_game_has_ended);
        }
        else {
            if (game.getPlayer2().isDroid()) {
                againstTextView.setText(R.string.droid_playing);
            } else {
                String resourceString = getResources().getString(R.string.two_humans_playing);

                // String formatting is included in the resourceString, messageToDisplay will replace
                // %s with the player whose turn it is
                String messageToDisplay = String.format(resourceString, game.getCurrentPlayingPlayer().getName());
                againstTextView.setText(messageToDisplay);
            }
        }
    }

    /**
     * Helper method to update the drawables of the image buttons to represent
     * the TicTacToe game board.
     * Updates the drawable of an ImageButton only if the Drawable that it contains does
     * not correspond to the mark of the appropriate Cell in the TicTacToe game
     */
    private void updateButtonDrawables() {
        Board board = game.getBoard();
        for(int col = 0; col < imageButtons.length; col++) {
            for (int row = 0; row < imageButtons[col].length; row++) {
                Cell currentCell = board.getCell(col, row);
                Mark cellMark = currentCell.getMark();

                ImageButton currentImageButton = imageButtons[col][row];
                Drawable buttonDrawable = currentImageButton.getDrawable();

                if (cellMark == Mark.NO_MOVE_YET && buttonDrawable != this.notPlayedDrawable) {
                    currentImageButton.setImageDrawable(this.notPlayedDrawable);
                } else if (cellMark == Mark.PLAYER_1 && buttonDrawable != this.player1MoveDrawable) {
                    currentImageButton.setImageDrawable(this.player1MoveDrawable);
                } else if (cellMark == Mark.PLAYER_2 && buttonDrawable != this.player2MoveDrawable) {
                    currentImageButton.setImageDrawable(this.player2MoveDrawable);
                } else if (cellMark == Mark.DROID && buttonDrawable != this.droidMoveDrawable) {
                    currentImageButton.setImageDrawable(this.droidMoveDrawable);
                }
            }
        }
    }

    /**
     * Helper method to update the clickable state (isEnabled boolean) of the image buttons to represent
     * appropriately the Cells in the TicTacToe game.
     * Updates the clickable state of an ImageButton only if the state does not correspond
     * to the necessary state represented by the appropriate state.
     * E.g. if there is a NOT_YET_PLAYED mark, the image button should be enabled.
     */
    private void updateButtonClickableState() {
        Board board = game.getBoard();
        for(int col = 0; col < imageButtons.length; col++) {
            for (int row = 0; row < imageButtons[col].length; row++) {
                Cell currentCell = board.getCell(col, row);
                Mark cellMark = currentCell.getMark();

                ImageButton currentImageButton = imageButtons[col][row];

                if (cellMark == Mark.NO_MOVE_YET && currentImageButton.isEnabled() == false) {
                    currentImageButton.setEnabled(true);
                } else if (cellMark == Mark.PLAYER_1 && currentImageButton.isEnabled()) {
                    currentImageButton.setEnabled(false);
                } else if (cellMark == Mark.PLAYER_2 && currentImageButton.isEnabled()) {
                    currentImageButton.setEnabled(false);
                } else if (cellMark == Mark.DROID && currentImageButton.isEnabled()) {
                    currentImageButton.setEnabled(false);
                }
            }
        }
    }

    /**
     * Helper method to display a message at the end of the game
     */
    private void displayEndOfGameMessage() {
        if (isEndOfGameDisplayed == false) {
            if (game.getGameState() == TicTacToe.GameState.TIE) {
                Toast.makeText(this, R.string.end_game_tie,
                        Toast.LENGTH_LONG).show();
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);

                builder.setMessage(String.format(getString(R.string.end_game_win), game.getPlayerThatWon().getName()))
                        .setTitle(R.string.end_game_dialog_title)
                        .setPositiveButton(R.string.end_game_dialog_ok_button,
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int which) {
                                        dialog.dismiss();
                                    }
                                })
                        .show();
            }
            isEndOfGameDisplayed = true;
        }
    }

    /**
     * Helper method to make all buttons unclickable (disabled)
     */
    private void disableAllButtons() {
        for (int col = 0; col < imageButtons.length; col++) {
            for (int row = 0; row < imageButtons[col].length; row++) {
                imageButtons[col][row].setEnabled(false);
            }
        }
    }

    /**
     * Helper method to increment the necessary counters depending on the
     * TicTacToe game state
     */
    private void incrementGameCounters() {
        if (game.getGameState() == TicTacToe.GameState.TIE)
            this.tieCount++;
        else if (game.getGameState() == TicTacToe.GameState.PLAYER_1_WON)
            this.player1WinCount++;
        else if (game.getGameState() == TicTacToe.GameState.PLAYER_2_WON && !game.getPlayerThatWon().isDroid())
            this.player2WinCount++;
        else if (game.getPlayerThatWon().isDroid())
            this.droidWinCount++;
    }
}
